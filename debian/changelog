golang-github-newrelic-go-agent (3.15.2-9) unstable; urgency=medium

  * Team upload
  * Increase timeout for flaky tests

 -- Shengjing Zhu <zhsj@debian.org>  Tue, 31 Jan 2023 16:02:54 +0800

golang-github-newrelic-go-agent (3.15.2-8) unstable; urgency=medium

  * Team upload
  * Simplify packaging
    + Exclude all integrations which are not used by others
    + Undo package split
  * Update Standards-Version to 4.6.2 (no changes)
  * Add Multi-Arch hint

 -- Shengjing Zhu <zhsj@debian.org>  Mon, 30 Jan 2023 17:28:12 +0800

golang-github-newrelic-go-agent (3.15.2-7) unstable; urgency=medium

  * Source-only upload

 -- Peymaneh <peymaneh@posteo.net>  Sun, 20 Nov 2022 00:19:18 +0100

golang-github-newrelic-go-agent (3.15.2-6) unstable; urgency=medium

  * d/changelog: Add Replaces/Breaks lines

 -- Peymaneh <peymaneh@posteo.net>  Sun, 20 Nov 2022 00:19:08 +0100

golang-github-newrelic-go-agent (3.15.2-5) unstable; urgency=medium

  * d/control: Fix Go import path

 -- Peymaneh <peymaneh@posteo.net>  Thu, 27 Oct 2022 15:05:08 +0200

golang-github-newrelic-go-agent (3.15.2-4) unstable; urgency=medium

  * split package
  * d/rules: Exclude example-files
  * d/rules: be less verbose
  * d/control: Update standards version

 -- Peymaneh <peymaneh@posteo.net>  Fri, 09 Sep 2022 19:20:06 +0200

golang-github-newrelic-go-agent (3.15.2-3) unstable; urgency=medium

  * Team Upload.
  * Ignore TestTraceObserverConsumeSpan that
    fails on armhf debci

 -- Nilesh Patra <nilesh@debian.org>  Thu, 17 Feb 2022 12:19:33 +0530

golang-github-newrelic-go-agent (3.15.2-2) unstable; urgency=medium

  * Team Upload.
  * Omit TestTrObsOKSendBackoffNo which chokes
    on debci but is OK locally

 -- Nilesh Patra <nilesh@debian.org>  Wed, 16 Feb 2022 13:36:00 +0530

golang-github-newrelic-go-agent (3.15.2-1) unstable; urgency=medium

  * d/patches: Fix patch
  * d/patches: Omit flaky test
  * New upstream version 3.15.2
  * d/control, d/copyright: Update maintainer info

 -- Peymaneh <peymaneh@posteo.net>  Sun, 19 Dec 2021 19:14:21 +0100

golang-github-newrelic-go-agent (3.15.1-1) unstable; urgency=medium

  * New upstream version 3.15.1
  * d/lintian-overrides: minor fix

 -- Peymaneh Nejad <p.nejad@posteo.de>  Sat, 20 Nov 2021 13:30:54 +0000

golang-github-newrelic-go-agent (3.15.0-1) unstable; urgency=medium

  [ Peymaneh Nejad ]
  * New upstream version 3.15.0
  * d/changelog: Remove whitespace
  * d/patches: Drop obsolete patch
  * d/lintian-overrides: Don't complain about examples/ dir
  * d/control: bump standards version

 -- Georg Faerber <georg@debian.org>  Sun, 05 Sep 2021 18:44:22 +0000

golang-github-newrelic-go-agent (3.14.0-2) unstable; urgency=medium

  * Testing migration

 -- Peymaneh Nejad <p.nejad@posteo.de>  Tue, 24 Aug 2021 14:58:48 +0200

golang-github-newrelic-go-agent (3.14.0-1) unstable; urgency=medium

  * Initial release (Closes: #991109)

 -- Peymaneh Nejad <p.nejad@posteo.de>  Thu, 15 Jul 2021 22:11:19 +0200
